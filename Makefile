PREFIX ?= /usr/local

.PHONY: clean
clean:

.PHONY: install
install:
	install -m 755 generate-ca $(PREFIX)/bin
	install -m 755 generate-cert $(PREFIX)/bin


.PHONY: uninstall
uninstall:
	rm -f $(PREFIX)/bin/generate-ca
	rm -f $(PREFIX)/bin/generate-cert

